package com.ract.clientlistener;

import java.net.*;
import java.io.*;
import java.util.*;

class SockData
{
    static  final   byte[] Read(BufferedInputStream inStream)
            throws  Exception
    {
        int     pSize       =   2000;
        byte[]  pData       = new byte[pSize]; // Maximum message size is 100 bytes
        int    thisByte;
        int     byteCount   =   0;

        thisByte = inStream.read ();

        while (     thisByte != 4       // End of Stream marker
                &&  thisByte != -1      // End of File
                &&  byteCount < pSize) // Too many characters
        {
            pData[byteCount] = (byte) thisByte;

            byteCount++;
            thisByte = inStream.read ( );
        }

        int pS = byteCount - 2;
        byte[] rData = new byte[pS + 1];
        try
        {
            for (byteCount =  0; byteCount <= pS ; byteCount++ )
            {
                rData[byteCount] = pData[byteCount];
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return rData;
    }

}
