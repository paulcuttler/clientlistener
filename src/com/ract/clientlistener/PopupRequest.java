package com.ract.clientlistener;

import java.util.logging.Logger;
import  java.rmi.*;
import java.util.*;
import java.net.*;
import java.io.*;

public class PopupRequest implements RemoteCommand
{
  private static final String commandLine  =   "\"C:\\Program Files\\Internet Explorer\\iexplore.exe\" ";
  private Logger logger=null;
  /**
   * executeCommand
   *
   * @param command String
   * @param logIt Logger
   */
  public void executeCommand(String commandRequest, Logger logIt) throws RemoteException
  {
      logger = logIt;
      logIt.info("PopupRequest = " + commandRequest);
      String commandStr = new String(commandRequest);
      if(!validRequest(commandStr))throw new RemoteException("Invalid popup request");
      String execCommand = commandLine + " " + new String(commandRequest);
      logIt.info(execCommand);
      try {
        Process proc = Runtime.getRuntime().exec(execCommand, null);
        String result = getProcOutput(proc);
        logIt.info(result);
        proc.waitFor();
        proc.destroy();
      }
      catch (Exception ex) {
        logIt.info("Error running " + execCommand + " / " + ex.getMessage());
        throw new RemoteException("Error running " + execCommand + " / " +
                                  ex.getMessage());
      }
  }
  /**
   * executeCommand
   *
   * @param command String
   */
  public void executeCommand(String command) {
  }

  private String getProcOutput(Process proc) throws Exception{
    // open output and error streams of the created process
    BufferedInputStream in = new BufferedInputStream(proc.getInputStream());
    BufferedInputStream err = new BufferedInputStream(proc.getErrorStream());
    // send output and error from the created process to client
    int nOut = 0;
    int nErr = 0;
    int nSize = 100;
    byte[] pBuffer = new byte[nSize];
    while (nOut >= 0 || nErr >= 0)
    {
        // read and send normal output
        nOut = in.read(pBuffer, 0, nSize);
        // read and send error output
        nErr = err.read(pBuffer, 0, nSize);
        // sleep a while to avoid burning too much CPU
        //   Thread.currentThread().sleep(100);
    }
      // clean up
    in.close();
    err.close();
    return new String(pBuffer);
  }
  /**
   * validRequest
   * @param cLine String
   * @return boolean
   * A valid popup request must be structured as follows:
   * Url of popup uic?event=requestName&userid=userid&requestSeq=requestNo
   * If the correct format is not followed, do not continue.
   */
  private boolean validRequest(String cLine)
  {
    String[] pNames = {"event","userid","requestSeq"};
    int qAt = cLine.indexOf('?');
    String url = cLine.substring(0,qAt);
    String parameterList = cLine.substring(qAt+1);
    String[] uParts = url.split("/");
    String uic = uParts[uParts.length - 1];
    if(!uic.equals("PopupUIC"))
    {
      logger.info("invalid uic requested:- " + uic);
      return false;
    }
    String[] pParts = parameterList.split("&");
    for(int xx=0;xx<3;xx++)
    {
      String parameter = pParts[xx];
      String pName = (parameter.split("="))[0];
      if(!pName.equals(pNames[xx]))
      {
         logger.info("invalid parameter:- " + pName);
          return false;
      }
    }
    return true;
  }
}
