package com.ract.clientlistener;

import java.rmi.*;
import java.util.logging.*;

/**
 * <p>Interface for processing an integration document.</p>
 * <p>Does not maintain a transactional context.</p>
 *
 * @author:   dgk  4/5/04
 * @version 1.0
 */
public interface RemoteCommand
{

  public    void    executeCommand ( String command )
            throws  RemoteException;

  public    void    executeCommand ( String command, Logger logIt )
            throws  RemoteException;
}
