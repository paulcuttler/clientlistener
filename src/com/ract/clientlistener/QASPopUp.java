package com.ract.clientlistener;

import  java.rmi.*;
import java.util.*;
import java.util.logging.*;
import java.net.*;
import java.io.*;


import  com.ract.clientlistener.*;

class   QASPopUp
        implements  RemoteCommand
{
//
    Logger logIt;
//
    private static final String qasPopUp    =   "f:\\qasclient\\qushown.exe qaprown 1";

    private static final String qasRunning  =   "c:\\windows\\system32\\tasklist.exe  /nh /fi \"Imagename eq qaprow*\"";
    
    private String execCommand;

    public void executeCommand ( String commandRequest )
        throws  RemoteException
    {
    }

    public void executeCommand ( String commandRequest, Logger logMe )
        throws  RemoteException
    {
        logIt = logMe;
        logIt.info("QASPopUp " + commandRequest );
        if (isQASRunning())
        {
//
//      QAPROWN is running. Pop the window up
//
            execCommand = qasPopUp + " \"" + new String(commandRequest) + "\"";
            logIt.info(execCommand);
            try
            {
                Process proc = Runtime.getRuntime().exec(execCommand,null);
                String result = getProcOutput ( proc );
                logIt.info ( result );
                proc.waitFor();
                proc.destroy();
            }
            catch ( Exception ex)
            {
                logIt.info("Error running " + execCommand + " / " + ex.getMessage() );
                throw new RemoteException ( "Error running " + execCommand + " / " + ex.getMessage() ); 
            }
        }
        else
        {
            logIt.info ( "QASPROWN is not running");
        }
    }

    private boolean isQASRunning ()
    {
        try
        {
        
            // check to see if QAPROWN is running
            //
            logIt.info("isQASRunning Command " + qasRunning);
            Process proc = Runtime.getRuntime().exec(qasRunning,null);
            proc.waitFor();
            String result = getProcOutput ( proc );
            logIt.info( "isQASRunning Result " + result );
            if ( result.indexOf("INFO:") > -1 )
            {
                // QAS not loaded
                logIt.info("isQASRunning QAS not running");
                return false;
            }

            return true;
         }
         catch ( Exception e)
         {
             logIt.info("Error in isQASRunning " + e.getMessage() );
             return false;
         }
    }

    private String getProcOutput ( Process proc )
    {
        // open output and error streams of the created process
        BufferedInputStream in = new BufferedInputStream(proc.getInputStream());
        BufferedInputStream err = new BufferedInputStream(proc.getErrorStream());
      // send output and error from the created process to client
        int nOut = 0;
        int nErr = 0;
        int nSize = 100;
        byte[] pBuffer = new byte[nSize];
        try
      {
  
        while(nOut>=0 || nErr>=0)
        {
            // read and send normal output
          
            nOut = in.read(pBuffer,0,nSize);

            // read and send error output
            nErr = err.read(pBuffer,0,nSize);

            // sleep a while to avoid burning too much CPU
         //   Thread.currentThread().sleep(100);
        }
        
        // clean up
        in.close();
        err.close();
    
      }
      catch ( Exception e)
      {
        logIt.info ( "getProcOutput " + e.getMessage() );
      }

        return  new String(pBuffer);

    }
}
