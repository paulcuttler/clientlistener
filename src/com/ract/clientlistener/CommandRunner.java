package com.ract.clientlistener;

import java.net.*;
import java.io.*;
import java.lang.*;
import java.rmi.RemoteException;
import java.util.*;
import java.util.logging.*;
import com.ract.opal.*;

import com.ract.clientlistener.*;

class   CommandRunner
        implements  Runnable
{
    static String m_strFilter [] = {"","",""};
    Socket m_sock;
    Logger logIt;
    private OPALWindow ow = null;

    private String execCommand;

    final public void run( )
    {
//        logIt.info("In command runner");
    	try
        {
            // get the ip address of the client
            String strClient = m_sock.getInetAddress().toString();

            // exit the thread if client ip address does not have
            // permission to access this server
            // (you can add more complicated security here)
          /*  if(!validRequestingIp(strClient))
            {

//                logIt.info("Connection closed " );
                m_sock.close();
                return;
            }*/
            // open input  streams to the client

            BufferedInputStream inStream = new BufferedInputStream(m_sock.getInputStream());

            try
            {
//                while(true)
//                {
                    logIt.info("Getting Client Request");
                    // receive the command from the client
                    
                    inStream.mark(1);
                    int fc = inStream.read();
                    if (fc==60)
                    {
                    	// is xml
//                        	className = "com.ract.clientlistener.OPALRunner";
//                        	requestRemainder = cr + ">";
                    	inStream.reset();
        				if(ow == null) ow = new OPALWindow();
        				ow.read(inStream);
                    }
                    else
                    {
                    	inStream.reset();
                        byte[] commandRequest = SockData.Read(inStream);
                        String cr = new String( commandRequest );
                    // print what has been received from the client
     /*                   System.out.println(       "Client '"
                                        +   strClient
                                        +   "' wants '"
                                        +   cr
                                        +   "'\r\n");*/
                        int i   =   cr.indexOf(" ");
                        String className   =   cr.substring(0,i);
                        String requestRemainder = cr.substring( i + 1,cr.length());
                        RemoteCommand rc = null;
                        Class classToRun = null;
                        try
                        {                  	
                           classToRun = Class.forName(className);
                           logIt.info("Got Class " + className );
                           rc = (RemoteCommand) classToRun.newInstance();
                           logIt.info("Instantiated Class " + className  + " / " + requestRemainder);
                           rc.executeCommand( requestRemainder, logIt );

                        }
                        catch (Exception ex)
                        {
                        	ex.printStackTrace();
                           logIt.info("Error running " + className + " - " + ex.getMessage() );
                        }
                    }

//                }
            }
            catch(Exception e)
            {
                logIt.info("Error in process execute " + e.getMessage() + " - " + strClient );
            }

            logIt.info("Closing CLient Connection");
            // close connection to client
            inStream.close();
            logIt.info("Closing Socket");
            m_sock.close();
            logIt.info("all closed");
        }
        catch(Exception e)
        {
            logIt.info("Error in run " + e.getMessage());
        }
    }

    private boolean validRequestingIp ( String ipAddress )
    {
              logIt.info("IP " + ipAddress);
      for (int i = 0; i < m_strFilter.length; i++)
      {
        if ( ipAddress.indexOf(m_strFilter[i]) > -1 )
        {
          return true;
        }
      }
      return false;
    }
}
