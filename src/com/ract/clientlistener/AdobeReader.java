package com.ract.clientlistener;

import  java.rmi.*;
import java.util.*;
import java.util.logging.*;
import java.net.*;
import java.io.*;

import  com.ract.clientlistener.*;

class   AdobeReader
        implements  RemoteCommand
{
//
    Logger logIt;
//
    private static final String AdobeReaderExe    	=   "AcroRD32.exe";
    private static final String adobeDirSpec = "F:\\ClientListener\\adobeReaderDirectories.txt";
    
    
	
  	private static String adobeExeLocation;
  	private static long   lastModified;
  	
    private String execCommand;
    
     public	AdobeReader ()
    {
    	adobeExeLocation	=	null;
    	lastModified		=	0;
    }
    public static void main (String[] args)
    {

        try
        {	
         	AdobeReader ar = new AdobeReader();
        	ar.findAdobeReader ( adobeDirSpec );
        	System.out.println("aaaa " + adobeExeLocation);
        	
        }
        finally
        {
        }
        
    }

    public void executeCommand ( String commandRequest )
        throws  RemoteException
    {
    }

    public void executeCommand ( String commandRequest, Logger logMe )
        throws  RemoteException
    {
    	findAdobeReader( adobeDirSpec );
        logIt = logMe;
        logIt.info("AdobeReader " + commandRequest );

        execCommand = adobeExeLocation + " /n " + new String(commandRequest) + "\"";
        logIt.info(execCommand);
        try
        {
            Process proc = Runtime.getRuntime().exec(execCommand,null);
            String result = getProcOutput ( proc );
            logIt.info ( result );
            proc.waitFor();
            proc.destroy();
        }
        catch ( Exception ex)
        {
        	logIt.info("Error running " + execCommand + " / " + ex.getMessage() );
            throw new RemoteException ( "Error running " + execCommand + " / " + ex.getMessage() ); 
        }
    	
    }
    
    public void scanAdobeDirs ( String adobeDir )
    {
    	File[] adobeReader = new File(adobeDir).listFiles();

    	if (adobeReader != null) 
        {
             for (File file : adobeReader) 
            {
            	if (	file.isDirectory() )
            	{
            		scanAdobeDirs ( file.getPath() );
            	}
            	else
            	{	
            		if (	file.getName().equalsIgnoreCase(AdobeReaderExe)
            			&&	file.lastModified() > lastModified
            		   )
            		{
             				lastModified 		= 	file.lastModified();
            				adobeExeLocation 	=	file.getAbsolutePath();
             		}
            	}
            }
        } 
        

    }

    public void findAdobeReader (String adobeDirSpec)
    {
    	try
    	{
    		FileInputStream fis 	= 	new FileInputStream ( adobeDirSpec );
    		BufferedReader	br 		= 	new BufferedReader(new InputStreamReader(fis));
    		String			dir;
    		
    		while	((dir = br.readLine()) != null )
    		{
    			scanAdobeDirs ( dir );
    		}
    	}
    	catch ( Exception e )
    	{
    		System.out.println(e.getMessage());
    		adobeExeLocation = null;
    	}
    	   	
    }

    private String getProcOutput ( Process proc )
    {
        // open output and error streams of the created process
        BufferedInputStream in = new BufferedInputStream(proc.getInputStream());
        BufferedInputStream err = new BufferedInputStream(proc.getErrorStream());
      // send output and error from the created process to client
        int nOut = 0;
        int nErr = 0;
        int nSize = 100;
        byte[] pBuffer = new byte[nSize];
        try
      {
  
        while(nOut>=0 || nErr>=0)
        {
            // read and send normal output
          
            nOut = in.read(pBuffer,0,nSize);

            // read and send error output
            nErr = err.read(pBuffer,0,nSize);

            // sleep a while to avoid burning too much CPU
         //   Thread.currentThread().sleep(100);
        }
        
        // clean up
        in.close();
        err.close();
    
      }
      catch ( Exception e)
      {
        logIt.info ( "getProcOutput " + e.getMessage() );
      }

        return  new String(pBuffer);

    }     

}
