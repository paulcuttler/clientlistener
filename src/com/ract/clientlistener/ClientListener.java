package com.ract.clientlistener;

import java.net.*;
import java.io.*;
import java.rmi.RemoteException;
import java.util.*;
import java.util.logging.*;

class ClientListener
{

private static Logger logger;

    public static void main (String[] args)
    {
        try
        {
            // Create a file handler that write log record to a file called my.log
            FileHandler handler = new FileHandler();

            // Add to the desired logger
            logger = Logger.getLogger("com.ract.clientlistener.ClientListener");
//            logger.addHandler(handler);
//            logger.info("Logging Started");
        }
        catch (IOException e)
        {
        }

        //
        //  Only allow commands from this ip to execute
        //
        CommandRunner.m_strFilter[0] = "203.20.20.26";
        CommandRunner.m_strFilter[1] = "203.20.20.141";
        CommandRunner.m_strFilter[2] = "127.0.0.1";

        int nPort = 38724;
        try
        {
            // start the server and listening for client connection at given port
            ServerSocket sockServer = new ServerSocket(nPort);

            while(true)
            {
                try
                {
//                    logger.info("ClientListener Setting up new CommandRunner");
                    CommandRunner cmdRunner = new CommandRunner();
//                    logger.info("ClientListener Setting up new CommandRunner - done");
                    cmdRunner.m_sock = sockServer.accept();
//                     logger.info("ClientListener Setting up new CommandRunner logger");
                    cmdRunner.logIt = logger;
//                    logger.info("ClientListener Starting CommandRunner");
                    new Thread(cmdRunner).start();
                    logger.info("ClientListener Back from CommandRunner");

                }
                catch(Exception inner)
                {
                    logger.info("Inner " + inner.getMessage());
                    Thread.currentThread().sleep(1000);
                }
            }

        }
        catch(Exception outer)
        {
            logger.info("Outer " + outer.getMessage());
        }

    }
    

}
